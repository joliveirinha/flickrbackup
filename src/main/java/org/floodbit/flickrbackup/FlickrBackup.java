package org.floodbit.flickrbackup;

import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.RequestContext;
import com.flickr4java.flickr.auth.Auth;
import com.flickr4java.flickr.auth.AuthInterface;
import com.flickr4java.flickr.auth.Permission;
import com.flickr4java.flickr.photos.Photo;
import com.flickr4java.flickr.photos.PhotosInterface;
import com.flickr4java.flickr.photos.Size;
import com.flickr4java.flickr.photosets.Photoset;
import com.flickr4java.flickr.photosets.PhotosetsInterface;
import com.flickr4java.flickr.util.AuthStore;
import com.flickr4java.flickr.util.FileAuthStore;
import org.floodbit.flickrbackup.exceptions.FlickrBackupException;
import org.scribe.model.Token;
import org.scribe.model.Verifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;


public class FlickrBackup {

    private final static Logger logger = LoggerFactory.getLogger(FlickrBackup.class);

    public static final String CONFIG_FILE = "config.properties";

    public static final String AUTH_DIR = "authStore";

    public static final String API_KEY = "apiKey";

    public static final String SECRET = "secret";

    private final File backupDir;

    private final ExecutorService executorService = Executors.newWorkStealingPool();

    private final Flickr flickr;

    private final AuthStore authStore;


    public FlickrBackup(final File destPath, final File configPath) throws FlickrBackupException {

        if (!destPath.isDirectory())
            throw new IllegalArgumentException(String.format("Destination Path %s is not a directory", destPath));

        this.backupDir = destPath;


        Properties conf = new Properties();
        File configFile = new File(configPath, CONFIG_FILE);

        try {
            conf.load(new BufferedReader(new FileReader(configFile)));
        } catch (FileNotFoundException e) {
            throw new FlickrBackupException(String.format("Config file %s not found. Please create it with Flickr API settings.",
                    configFile.getAbsolutePath()), e);
        } catch (IOException e) {
            throw new FlickrBackupException("Error reading config file!", e);
        }

        this.flickr = new Flickr(conf.getProperty(API_KEY), conf.getProperty(SECRET), new REST());

        // now configure the auth store
        File authDir = new File(configPath, AUTH_DIR);
        if (!authDir.exists())
            authDir.mkdir();

        try {
            authStore = new FileAuthStore(authDir);
        } catch (FlickrException e) {
            throw new FlickrBackupException(e.getMessage(), e);
        }

    }

    private void authorize() throws FlickrBackupException {

        AuthInterface authInterface = flickr.getAuthInterface();

        Token accessToken = authInterface.getRequestToken();

        String url = authInterface.getAuthorizationUrl(accessToken, Permission.READ);
        logger.info("Follow this URL to authorise yourself on Flickr");
        logger.info(url);
        logger.info("Paste in the token it gives you:");
        logger.info(">>");

        String tokenKey = new Scanner(System.in).nextLine();

        Token requestToken = authInterface.getAccessToken(accessToken, new Verifier(tokenKey));

        try {
            Auth auth = authInterface.checkToken(requestToken);
            RequestContext.getRequestContext().setAuth(auth);
            authStore.store(auth);

            logger.info("Application successful authorized...");

        } catch (FlickrException | IOException e) {
            throw new FlickrBackupException("Error getting authorization", e);
        }
    }


    public void doBackup() throws FlickrBackupException {
        logger.info("Starting backup...");

        RequestContext context = RequestContext.getRequestContext();

        Auth[] auths = authStore.retrieveAll();
        if (auths.length == 0) {
            authorize();
        } else  {
            context.setAuth(auths[0]);
        }

        String nis = context.getAuth().getUser().getId();
        PhotosetsInterface setsInterface = flickr.getPhotosetsInterface();

        List<BackupPhoto> photos= new ArrayList();
        try {

            for (Photoset s : setsInterface.getList(nis).getPhotosets()) {
                for (Photo p : getPhotosFromSet(setsInterface, s))
                    photos.add(new BackupPhoto(s, p));
            }

            // add photos not in set
            Photoset notInSet = new Photoset();
            notInSet.setTitle("notInSet");

            for (Photo p: getPhotosNotInSet()) {
                photos.add(new BackupPhoto(notInSet, p));
            }

        } catch (FlickrException e) {
            throw new FlickrBackupException(e.getMessage(), e);
        }

        logger.info("Metadata retrieved... Starting to download...");

        List<BackupPhoto> filteredPhotos = photos.stream()
                .filter(p -> !p.isAlreadyBacked())
                .collect(Collectors.toList());

        logger.info("{} photos from a total of {} are already backed up! " +
                        "Submitting {} remaining photos do download now...",
                photos.size() - filteredPhotos.size(),
                photos.size(),
                filteredPhotos.size());

        List<Future> futures = filteredPhotos.stream()
                .map(p -> executorService.submit(() -> downloadPhoto(p)))
                .collect(Collectors.toList());

        futures.forEach(f -> {
            try {
                f.get();
            } catch (Exception e) {
                logger.error("Error running task.");
            }
        });

    }

    private List<Photo> getPhotosNotInSet() throws FlickrException {
        List<Photo> list = new ArrayList();
        final int photosPerPage = 500;
        PhotosInterface pi = flickr.getPhotosInterface();

        int page = 0;

        do {
            page++;
            List<Photo> l = pi.getNotInSet(photosPerPage, page);
            list.addAll(l);
        } while (list.size() == photosPerPage);

        return list;
    }

    private void downloadPhoto(BackupPhoto p) {
        logger.info("Starting to download photo {}", p);

        try {
            InputStream is = p.getInputStream();
            BufferedOutputStream bos = p.getOutputStream();

            int b;
            while ((b = is.read()) != -1) {
                bos.write(b);
            }

            bos.close();
            is.close();
        } catch (IOException | FlickrException e) {
            logger.error("Error downloading photo!", e);
        }
    }

    private List<Photo> getPhotosFromSet(PhotosetsInterface pi, Photoset set) throws FlickrException {
        List<Photo> list = new ArrayList();
        final int photosPerPage = 500;

        for (int p=1; p <= Math.ceil(set.getPhotoCount()/(double)photosPerPage) ;p++) {
            list.addAll(pi.getPhotos(set.getId(), photosPerPage, p));
        }

        logger.info("Retrieved {} of {} photos within Photoset {}",
                set.getPhotoCount() == 0 ? 0 : (list.size()/(double)set.getPhotoCount())*100,
                set.getPhotoCount(),
                set.getTitle());

        return list;
    }

    private class BackupPhoto {

        private final Photoset set;
        private final Photo photo;

        public BackupPhoto(Photoset set, Photo p) {
            this.set = set;
            this.photo = p;
        }

        public boolean isAlreadyBacked() {
            return new File(getBackupFilename()).exists();
        }

        public InputStream getInputStream() throws FlickrException {
            return flickr.getPhotosInterface().getImageAsStream(photo, Size.ORIGINAL);
        }

        private BufferedOutputStream getOutputStream() throws IOException {
            File f = new File(getBackupFilename());

            // make sure we are able to write.. creating dirs.
            f.getParentFile().mkdirs();

            return new BufferedOutputStream(new FileOutputStream(f));
        }

        private String getBackupFilename() {
            File setPath = new File(backupDir, safeFilename(set.getTitle()));
            File photoFile = new File(setPath, safeFilename(photo.getId())+".jpg");

            return photoFile.getAbsolutePath();
        }

        private String safeFilename(String filename) {
            return filename.replaceAll("[^a-zA-Z0-9-_\\.]", "_");
        }

        @Override
        public String toString() {
            return String.format("{ID# %s | Name# %s | Loc: %s}",
                    photo.getId(),
                    photo.getTitle(),
                    getBackupFilename());
        }
    }


}
