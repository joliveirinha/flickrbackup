package org.floodbit.flickrbackup.exceptions;

/**
 * Created by neo on 17/01/15.
 */
public class FlickrBackupException extends Exception {

    public FlickrBackupException(String message) {
        super(message);
    }

    public FlickrBackupException(String message, Exception e) {
        super(message, e);
    }

}
