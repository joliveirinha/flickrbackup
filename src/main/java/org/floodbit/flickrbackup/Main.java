package org.floodbit.flickrbackup;


import org.floodbit.flickrbackup.exceptions.FlickrBackupException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Arrays;

public class Main {

    private final static Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        if (args.length < 1) {
            logger.info("Usage: flickrbackup <destPath>");
            System.exit(1);
        }

        String path = args[0];
        File configPath = new File(System.getProperty("user.home"), ".flickrbackup");

        try {
            FlickrBackup b = new FlickrBackup(new File(path), configPath);
            b.doBackup();
        } catch (FlickrBackupException e) {
            logger.error(e.getMessage());
            System.exit(1);
        }
    }
}
