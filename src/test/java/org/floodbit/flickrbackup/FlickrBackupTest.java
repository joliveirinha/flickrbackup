package org.floodbit.flickrbackup;

import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.util.AuthStore;
import mockit.Mock;
import mockit.MockUp;
import org.floodbit.flickrbackup.exceptions.FlickrBackupException;
import org.junit.Test;

import java.io.File;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.Assert.*;

public class FlickrBackupTest {

    public FlickrBackup setup() throws Exception {

        File tmp = File.createTempFile("flickrbackup_dest", null);
        tmp.delete();
        tmp.mkdir();

        File conf = new File(getClass().getResource("/config").getPath());


        return new FlickrBackup(
                tmp,
                conf
        );
    }

    @Test(expected=FlickrBackupException.class)
    public void testNoConfig() throws Exception {
        File tmp = File.createTempFile("flickrbackup_dest", null);
        tmp.delete();
        tmp.mkdir();

        File authStore = File.createTempFile("flickrbackup", null);
        authStore.delete();
        authStore.mkdir();

        new FlickrBackup(
                tmp,
                authStore
        );
    }

    @Test(expected = AssertionError.class)
    public void testBackupWithoutAuth() throws Exception {

        new MockUp<Scanner>() {

            @Mock
            private String nextLine() {
                return "a";
            }
        };

        FlickrBackup b = setup();
        b.doBackup();

    }
    


}
